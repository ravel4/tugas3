package Tugas3;

public class MataKuliah {
    private String nomor;
    private String namaMK;
    private int nilaiAngka;

    public MataKuliah(String nomor, String namaMK, int nilaiAngka) {
        this.nomor = nomor;
        this.namaMK = namaMK;
        this.nilaiAngka = nilaiAngka;
    }

    public String getNomor() {
        return nomor;
    }

    public String getNamaMatkul() {
        return namaMK;
    }

    public String getNilaiHuruf() {
        if (nilaiAngka >= 80) {
            return "A";
        } else if (nilaiAngka >= 70) {
            return "B";
        } else if (nilaiAngka >= 60) {
            return "C";
        } else if (nilaiAngka >= 50) {
            return "D";
        } else {
            return "E";
        }
    }
}