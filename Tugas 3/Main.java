package Tugas3;

import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan NIM mahasiswa: ");
        String nim = scanner.nextLine();
        System.out.print("Masukkan nama mahasiswa: ");
        String nama = scanner.nextLine();

        Mahasiswa mahasiswa = new Mahasiswa(nim, nama);
        System.out.println();
        System.out.println("Lengkapi berkas berikut...");
        boolean isiLagi = true;
        while (isiLagi) {
            System.out.print("Kode mata kuliah: ");
            String nomor = scanner.nextLine();

            System.out.print("Nama mata kuliah: ");
            String namaMK = scanner.nextLine();

            System.out.print("Nilai: ");
            int nilaiAngka = scanner.nextInt();
            scanner.nextLine();

            System.out.println();
            mahasiswa.listMatKul(new MataKuliah(nomor, namaMK, nilaiAngka));
            System.out.println("Input Mata Kuliah lain? (y/n)");
            String next = scanner.next();
            scanner.nextLine();
            if (next.equalsIgnoreCase("n")) isiLagi=false;
        }

        mahasiswa.printMatkul();
        scanner.close();
    }
}