package Tugas3;

import java.util.ArrayList;
public class Mahasiswa {
    private String nim;
    private String nama;
    private ArrayList<MataKuliah> listMatKuls;

    public Mahasiswa(String nim, String nama) {
        this.nim = nim;
        this.nama = nama;
        this.listMatKuls=new ArrayList<>();
    }

    public void listMatKul(MataKuliah mataKuliah){
        listMatKuls.add(mataKuliah);
    }

    public void printMatkul(){
        System.out.println("NIM\t\t: " + nim);
        System.out.println("Nama\t\t: " + nama);
        System.out.println();
        System.out.println("Matkul yang diambil\t: ");
        for (MataKuliah mataKuliah : listMatKuls ){
            System.out.printf("|%-10s|%-20s|%-5s|\n",mataKuliah.getNomor(),mataKuliah.getNamaMatkul(), mataKuliah.getNilaiHuruf());
        }
    }
}
